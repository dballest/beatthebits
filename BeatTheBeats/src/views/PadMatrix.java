package views;

import java.io.IOException;

import models.Pad;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TableLayout;
import android.widget.TableRow;


public class PadMatrix extends TableLayout {

	int N;
	private Pad pad;
	private TileButton buttons[];
	private boolean isInEditMode = false;

	/* Constructores */
	public PadMatrix(Context context) {
		super(context);
	}
	
	public PadMatrix(Context context, AttributeSet attrs ) {
		super(context, attrs);
	}

	public void be( Context context, Pad model ) throws IllegalArgumentException, SecurityException, IllegalStateException, IOException {
		
		pad = model;
		N = pad.getSize() * pad.getSize();
		buttons = new TileButton[ N ];
		
		if( pad != null ) {
			
			pad.loadTilesFromDatabase( context );

			TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
					TableRow.LayoutParams.WRAP_CONTENT, 1);

			int n = 0;
			for( int i = 0; i < pad.getSize(); i ++ ){
				TableRow tr = new TableRow( context );
				tr.setLayoutParams( lp );
				for ( int j = 0; j < pad.getSize(); j++ ) {				
					buttons[n] = new TileButton( context, 150 );
					buttons[n].setIndex( n );
					if( pad.getTile(n) != null ) 
						buttons[n].fillModel( pad.getTile(n) );
					tr.addView( buttons[n] );
					n++;
				}
				this.addView(tr);
			}
		}
	}

	public void release() {
		for( int n = 0; n < N; n++ ){
			this.buttons[n].release();
		}	
	}

	public void correctTileSize( int pixels ) {

		for( int n = 0; n < N; n++ ){
			buttons[n].setWidth( pixels );
			buttons[n].setHeight( pixels );
		}
	}

	public void selectTile() {
		if(isInEditMode) {
			for( int n = 0; n < N; n++ ){
				buttons[n].removeOnSelectListener();
			}
			isInEditMode = false;
		} else {
			isInEditMode = true;
			for( int n = 0; n < N; n++ ){
				buttons[n].addOnSelectListener();
			}
		}
	}

	public void setTrackTo(int index, String path) 
			throws IllegalArgumentException,
			SecurityException,
			IllegalStateException,
			IOException {
		buttons[index].setTrack(path);
	}

	public boolean isInEditMode() {
		return isInEditMode;
	}
}
