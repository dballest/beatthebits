package views;

import java.io.IOException;

import models.Tile;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.TableRow.LayoutParams;

import com.beatthebeats.android.R;

import controllers.TrackList;

public class TileButton extends Button implements OnTouchListener {
	
	private Tile model = new Tile(); //debe haber aunque sea un tile vacio 
	MediaPlayer track;
	Context context;
	
	/* Constructores */
	public TileButton( Context context ) {
		super(context);
		this.context = context;
	}

	public TileButton( Context context, int pixels ) {
		super(context);
		this.context = context;
		/* Coloreamos de gris por defecto
		 * y le damos tamanio estandar */
		defaultStyle(pixels);
	}

	/**
	 *  Logica del escuchador
	 * @return true porque si
	 */
	@Override
	public boolean onTouch(View view, MotionEvent event) {
		switch( event.getAction() ) {
		case android.view.MotionEvent.ACTION_DOWN:
			Log.d("TouchTest", "Touch down");
			track.start();
			if ( track.isPlaying() )
				view.setBackground( 
						getContext().getResources().
						getDrawable(R.drawable.playing_tile));
			break;
		case android.view.MotionEvent.ACTION_UP:
			Log.d("TouchTest", "Touch up");
			if ( track.isPlaying() && track.getDuration() > 2000 ) {
				track.pause();
				track.seekTo(0);
				view.setBackground( 
						getContext().getResources().
						getDrawable(R.drawable.playable_tile));
			}
		}
		return true;
	}

	/* Setters & Getters*/
	public void fillModel( Tile tile ) throws IllegalArgumentException, SecurityException, IllegalStateException, IOException{
		
		model = tile;
		this.setTrack( tile.getTrack() );
	}
	
	public int    getIndex() { 
		return model.getIndex();
	}
	
	public void   setIndex( int e ) { 
		model.setIndex(e);
	}
	
	public void   setPadId( int pad_id ) {
		model.setPadId(pad_id);
	}
	
	public int getPadId() {
		return model.getPadId();
	}
	
	public MediaPlayer getTrack() {
		return this.track;
	}
	
	public void   setTrack(String path) throws IllegalArgumentException, SecurityException,	IllegalStateException, IOException {
		/* Modelo y base de datos:*/
		model.setTrack(path);
		/* Resto: */
		track = new MediaPlayer();
		track.setDataSource( path );
		track.prepare();
		track.setLooping( true );
		this.setBackgroundResource( R.drawable.playable_tile );
		this.setOnTouchListener(this);
	}
	
	public void defaultStyle( int pixels ) {
		
		LayoutParams params = new LayoutParams(
		        LayoutParams.WRAP_CONTENT,      
		        LayoutParams.WRAP_CONTENT
		);
		params.setMargins(5, 5, 5, 5);
		
		this.setLayoutParams(params);
		this.setWidth(pixels);
		this.setHeight(pixels);
		this.setBackgroundResource( R.drawable.inactive_tile );
		
	}
	
	public void release() {
		track.release();		
	}
	
	public void addOnSelectListener() {
			
		this.setBackgroundResource( R.drawable.selectable_tile );
		this.setText( Integer.toString(model.getIndex()) );
		this.setOnTouchListener(null);
		this.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent( context, TrackList.class);
				Bundle b = new Bundle();
				b.putInt("index", model.getIndex() );
				myIntent.putExtras(b);
				context.startActivity(myIntent);
			}
		});
	}
	
	public void removeOnSelectListener() {
		this.setText( "" );
		this.setOnClickListener(null);
		if( isPlayable() ) {
			this.setOnTouchListener(this);
			this.setBackgroundResource( R.drawable.playable_tile );
		} else {
			this.setBackgroundResource( R.drawable.inactive_tile );
		}
	}
	
	public boolean isPlayable() {
		return track != null;
	}
}
