package controllers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.beatthebeats.android.R;

public class GeneralSettings extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_general_settings);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override 
	public boolean onOptionsItemSelected(MenuItem item){

		Intent newIntent;	
		switch(item.getItemId()){

		//Controlamos cual es la opcion del menu que se pulsa para hacer la accion que toque
		case R.id.action_profile:

			try {

				newIntent = new Intent( this,Profile.class );
				newIntent.putExtras( getIntent().getExtras());
				this.startActivity(newIntent);

			} catch( Exception e ) {

				Toast.makeText(getApplicationContext(),"No estas logueado",
						Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}

			break;
		case R.id.action_logout:
			newIntent = new Intent( this, Login.class);
			this.startActivity(newIntent);
			finish();
			break;

		default:
			break;
		}
		
		return true;
	}

}
