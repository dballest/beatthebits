package controllers;

import models.Pad;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;


public class RowClickListener implements OnClickListener{
	
	Context mContext;
	Pad pad;
    
    public RowClickListener(Context mContext, Pad pad){
    	this.mContext = mContext;
    	this.pad = pad;
    }

	@Override
	public void onClick(View view) {
		Intent intent = new Intent(mContext, PadController.class);
		Bundle b = new Bundle();
		b.putString("pad_title", "#" + pad.getId()  +" " + pad.getName() );
		b.putInt("pad_id", pad.getId());
//		b.putParcelable("color", (Parcelable) view.getBackground());
		intent.putExtras(b);
		mContext.startActivity(intent);
	}
}