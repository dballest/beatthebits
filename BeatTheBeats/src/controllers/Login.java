package controllers;

import models.User;
import helpers.Session;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.beatthebeats.android.R;


public class Login extends Activity {

	/**
	 * Activity de la clase
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
				
		/**
		 * Primero comprobamos si se han logueado ya 
		 **/
		skipActivityIfLogged();
		


		getActionBar().setIcon(R.drawable.alien);
		setTitle("Beat the Beats");
		setContentView(R.layout.activity_login);

		Button off_session_btn = 
				(Button) findViewById(R.id.off_session_btn);
		
		Button login_button = 
				(Button) findViewById(R.id.login_btn);
		
		login_button.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(Login.this, MainPageListController.class);
				String username = ((EditText) findViewById(R.id.login_username)).getText().toString();
				String password = ((EditText) findViewById(R.id.login_password)).getText().toString();
				((Session) getApplication()).user = new User(username, password);
				/* Salvar el nombre en preferencias */
				SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = sharedPref.edit();
				editor.putString( "user_name", username );
				editor.commit();
				/* Lanzar la siguiente activity */
				startActivity(myIntent);
				finish();
			}
		});

		off_session_btn.setOnClickListener(new View.OnClickListener() {
			
			@Override 
			public void onClick(View v) {
				Intent myIntent = new Intent( Login.this, PadController.class);
				startActivity(myIntent);
				overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
			}
		});
	} 
	
	private void skipActivityIfLogged(){
		
		Context context = getApplicationContext();
		SharedPreferences preferences = context.getSharedPreferences( "user" 
				, Context.MODE_PRIVATE);
		String username = preferences.getString("user_name", null);
		
		if( username == null ) {
			return; //no se salta la pantalla
		} else {
			Intent myIntent = new Intent(Login.this, MainPageListController.class);
			Bundle b = new Bundle();
			b.putString("user", username);
			myIntent.putExtras(b);
			startActivity(myIntent);
			finish();		
		}		
	}
}
