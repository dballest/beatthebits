package controllers;

import helpers.MySQLiteHelper;
import helpers.Session;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import models.Pad;
import models.Tile;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.beatthebeats.android.R;

public class TrackList extends Activity {

	private ListView list_view;
	private SimpleCursorAdapter cursor_adapter = null;
	private ArrayAdapter<String> array_adapter = null;
	private boolean assets = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_track_list);

		//Creamos la lista
		list_view = (ListView) findViewById(R.id.track_list);

		createList();              

		Button tracklist_btn = 
				(Button) findViewById(R.id.track_list_btn);

		tracklist_btn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				/*Intent myIntent = new Intent( TrackList.this, GeneralSettings.class);
                TrackList.this.startActivity(myIntent);*/
				if(assets){
					assets = false;
					createList();                   
				}else{
					assets = true;
					createList();                   
				}

			}
		});

	}

	private void createList(){

		if(assets){
			if(array_adapter == null){
				String[] file_names;
				List<String> track_list = null;

				try {
					file_names = getAssets().list("samples");
					track_list = Arrays.asList(file_names);

				} catch (IOException e) {
					Toast.makeText(getApplicationContext(), "Impossible to load default samples :(",
							Toast.LENGTH_LONG).show();
				}

				array_adapter = new ArrayAdapter<String>( this, android.R.layout.simple_list_item_1, track_list );
			}

			list_view.setAdapter( array_adapter );

			list_view.setOnItemClickListener( new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View view,
						int position, long id) {

					Toast.makeText(getApplicationContext(), "cancion de assets llamada: "+array_adapter.getItem(position),
							Toast.LENGTH_LONG).show();
				}   
			});

		} else{
			if(cursor_adapter == null){
				//http://www.androidsnippets.com/list-all-music-files
				String[] projection = {
						MediaStore.Audio.Media._ID,
						MediaStore.Audio.Media.TITLE,
						MediaStore.Audio.Media.DATA,
						MediaStore.Audio.Media.DISPLAY_NAME,
						MediaStore.Audio.Media.DURATION
				};

				Cursor cursor = getContentResolver().query(
						MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
						projection,
						null,
						null,
						MediaStore.Audio.Media.TITLE);

				//http://www.java2s.com/Code/Android/Media/ListtheMediaStore.htm
				String[] displayFields = new String[] { MediaStore.Audio.Media.TITLE };

				int[] displayViews = new int[] { android.R.id.text1 };

				cursor_adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, cursor, displayFields, displayViews,0);
			}

			list_view.setAdapter(cursor_adapter);

			list_view.setOnItemClickListener( new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View view,
						int position, long id) {

					// 1. Coger los datos necesarios
					Cursor cursor = cursor_adapter.getCursor();            
					cursor.moveToPosition(position);
					String trackpath = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));

					// 2. Guardar el tile en base de datos
					Bundle extras = getIntent().getExtras();
					Pad pad = ((Session) getApplicationContext()).pad;
					Tile tile = new Tile( extras.getInt("index"), 
							pad.getId(),
							trackpath);
					new MySQLiteHelper(getApplicationContext()).addTile(tile);

					// 3. Crear el intent y volver a la vista anterior
					extras.putInt( "pad_id", pad.getId() );
					extras.putString( "pad_title", pad.getName() );
					Intent newIntent = new Intent( getApplicationContext(), PadController.class);
					newIntent.putExtras(extras);
					// # FLAG para tareas fuera de una activity
					newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
					getApplicationContext().startActivity( newIntent );
				}   
			});
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item){

		Intent newIntent;

		switch(item.getItemId()){

		//Controlamos cual es la opcion del menu que se pulsa para hacer la accion que toque
		case R.id.action_profile:

			try {

				newIntent = new Intent( this,Profile.class );
				newIntent.putExtras( getIntent().getExtras());
				this.startActivity(newIntent);

			} catch( Exception e ) {

				Toast.makeText(getApplicationContext(),"No estas logueado",
						Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}

			break;

		case R.id.action_logout:
			newIntent = new Intent(this,Login.class);
			this.startActivity(newIntent);
			finish();
			break;

		case R.id.action_settings:
			newIntent = new Intent( this, GeneralSettings.class);
			this.startActivity(newIntent);
			finish();
			break;

		default:
			break;

		}

		return true;
	}
}