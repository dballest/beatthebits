package controllers;

import helpers.MySQLiteHelper;
import helpers.Session;

import java.util.ArrayList;

import models.Pad;
import models.User;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.beatthebeats.android.R;

public class MainPageListController extends Activity {

	//1. Cargo la base de datos en la actividad
	MySQLiteHelper db;

	//LIST OF ARRAY STRINGS WHICH WILL SERVE AS LIST ITEMS
	ArrayList<Pad> listItems;

	User user;
	Intent activity_intent;
	private ListView listView1;
	private String title;
	private RowAdapter adapter;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.activity_main_page);
		
		//1. Start db
		db = new MySQLiteHelper(this);
		
		//2. get all books
		listItems = db.getAllPads();

		this.adapter = new RowAdapter(this, 
				R.layout.list_row, listItems);

		listView1 = (ListView)findViewById(R.id.list);
		listView1.setAdapter(adapter);
	}

	private void showTitleDialog(){
		
		LayoutInflater factory = LayoutInflater.from(this);
    	final View padFeaturesSelector = factory.inflate(R.layout.pad_size_selector, null);
    	final EditText input = (EditText) padFeaturesSelector.findViewById(R.id.pad_title);
    	final Spinner sizeSelector = (Spinner) padFeaturesSelector.findViewById(R.id.pad_sizes_spinner);
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setTitle("Create New Pad");
    	
    	builder.setView(padFeaturesSelector);
  

		// Set up the buttons
		builder.setPositiveButton("Create", new DialogInterface.OnClickListener() { 
			@Override
			public void onClick(DialogInterface dialog, int which) {
				title = input.getText().toString();
				Pad pad = new Pad(title, "you");
				pad.setSize( getSelectedSize(sizeSelector) );
				db.addPad( pad );
				listItems.add( db.getPad( title ) );
				adapter.notifyDataSetChanged();
			}
		});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		builder.show();
	}

	//Controla el pulsado de los elementos de la lista
	public void onListItemClick(ListView parent, View v, int position, long id) {
		Intent load_intent = new Intent(this, PadController.class);
		load_intent.putExtras(activity_intent.getExtras());
		this.startActivity(load_intent);

	}
	
	@Override
	public void onResume(){
		super.onResume();
		
		Session session = ((Session) getApplication());
		session.loadPreferences();
		user = session.user;
		setTitle( user.getName() + "'s Home");
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.home_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item){

		Intent newIntent;
		switch(item.getItemId()){

		//Controlamos cual es la opcion del menu que se pulsa para hacer la accion que toque
		case R.id.action_profile2:
			if (activity_intent.getExtras()!=null){
				newIntent = new Intent(this, Profile.class);
				newIntent.putExtras(activity_intent.getExtras());
				this.startActivity(newIntent);
			} else {
				Toast.makeText(getApplicationContext(),"No estas logueado",
						Toast.LENGTH_LONG).show();
			}
			break;
		case R.id.action_logout2:
			newIntent = new Intent(this,Login.class);
			this.startActivity(newIntent);
			/* Borrar al usuario de las preferencias de aplicacion */
			SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = sharedPref.edit();
			editor.remove("user_name");
			editor.commit();

			finish();
			break;
		case R.id.action_settings2:
			newIntent = new Intent(this, GeneralSettings.class);
			this.startActivity(newIntent);
			break;
		case R.id.addBtn:
			showTitleDialog();
			break;
		default:
			break;
		}
		return true;
	}
	
    private int getSelectedSize(Spinner spinner){
    	String selection = spinner.getSelectedItem().toString();
    	if (selection.equalsIgnoreCase("3x3")){
    		return 3;
    	}else{
    		return 4;
    	}
    }

}