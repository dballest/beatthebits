package controllers;

import java.util.ArrayList;

import models.Pad;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.beatthebeats.android.R;

public class RowAdapter extends ArrayAdapter<Pad>{

	Context context;
	int LayoutResourceId;
	ArrayList<Pad> data;
	
	public RowAdapter(Context context, int layoutResourceId) {
		super(context, layoutResourceId);
		this.LayoutResourceId = layoutResourceId;
		this.context = context;
	}
	
	public RowAdapter(Context context, int layoutResourceId, ArrayList<Pad> data) {
		super(context, layoutResourceId, data);
		this.data = data;
		this.LayoutResourceId = layoutResourceId;
		this.context = context;
	}
	
	public View getView (int position, View convertView, ViewGroup parent){
		View row = convertView;
		RowHolder holder = null;
		if (row==null){
			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			row = inflater.inflate(LayoutResourceId, parent, false);
			
			holder = new RowHolder();
            holder.title = (TextView)row.findViewById(R.id.row_title);
            
            row.setTag(holder);
            row.setOnClickListener(new RowClickListener( context, data.get(position) ));
		}else{
			holder = (RowHolder)row.getTag();
		}
		
		Pad pr = data.get(position);
		row.setBackgroundColor(Color.parseColor(pr.getColor()));
		holder.title.setText("#"+pr.getId()+" "+pr.getName());
		return row;
	}
	
	
	static class RowHolder {
		TextView title;
	}
}