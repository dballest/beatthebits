package controllers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.EditText;

import com.beatthebeats.android.R;

public class Profile extends Activity {
	
	Bundle intent_extras;
	Intent activity_intent;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}  
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getActionBar().setIcon(R.drawable.alien);
		setTitle("Profile");
		setContentView(R.layout.activity_profile);
		activity_intent = getIntent();
		intent_extras = activity_intent.getExtras();
		
		EditText user_name_field = 
				(EditText) findViewById(R.id.name_field);
		
		if (intent_extras!=null){
			user_name_field.setText(intent_extras.getString("user"));
		}
	} 
}