package controllers;

import models.Pad;
import views.PadMatrix;
import helpers.MySQLiteHelper;
import helpers.Session;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.beatthebeats.android.R;

public class PadController extends Activity {

	Pad model;
	PadMatrix pad;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Set Home button
		// http://developer.android.com/guide/topics/ui/actionbar.html 
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		// Set the hardware buttons to control the music
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		setContentView(R.layout.activity_pad);
		
		model = new Pad(); //modelo vacio
		Intent myIntent = getIntent();
		Bundle extras = myIntent.getExtras();
		if (extras!=null){
			model.setName( extras.getString("pad_title") );
			model.setId( extras.getInt("pad_id") );		
			MySQLiteHelper db = new MySQLiteHelper( this );
			model = db.getPad( model.getId() ); //actualizar pad
			pad = (PadMatrix) findViewById(R.id.pad_table);
			try {
				pad.be(this, model);
				db.close();
			} catch ( Exception e) {
				e.printStackTrace();
			}
			((Session) getApplication()).pad = model;
			this.setTitle( model.getName() );
		}else{
			this.setTitle("Awesome Pad");
		}
		
		/**
		 * Medimos el pad
		 *
		pad.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		int _width = pad.getMeasuredWidth();
		int _height = pad.getMeasuredHeight();
		Log.d("Midiendo", "Width: "+_width+"px; Height: "+_height+"px;");
		 */
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pad_menu, menu);
		return true;
	}

	@Override 
	public boolean onOptionsItemSelected(MenuItem item){

		Intent newIntent;
		switch(item.getItemId()){

		//Controlamos cual es la opcion del menu que se pulsa para hacer la accion que toque
		case android.R.id.home:
			newIntent = new Intent( this, MainPageListController.class );
			newIntent.putExtras( getIntent().getExtras());
			this.startActivity(newIntent);
			break;

		case R.id.action_edit:
			pad.selectTile();
			break;

		case R.id.action_remove:
			MySQLiteHelper db = new MySQLiteHelper(this);
			db.deletePad( model );
			this.startActivity( new Intent( this, MainPageListController.class ) );
			break;

		case R.id.action_profile:
			try {
				newIntent = new Intent( this,Profile.class);
				newIntent.putExtras( getIntent().getExtras());
				this.startActivity(newIntent);
			} catch( Exception e ) {
				Toast.makeText(getApplicationContext(),"No estas logueado",
						Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
			break;

		case R.id.action_logout:
			newIntent = new Intent(this,Login.class);
			this.startActivity(newIntent);
			/* Borrar al usuario de las preferencias de aplicacion */
			SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = sharedPref.edit();
			editor.remove("user_name");
			editor.commit();

			finish();
			break;

		case R.id.action_settings:
			newIntent = new Intent(PadController.this, GeneralSettings.class);
			this.startActivity(newIntent);
			finish();
			break;

		default:
			break;
		}

		return true;
	}
}
