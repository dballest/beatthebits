package helpers;

import models.Pad;
import models.User;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;


public class Session extends Application{
	
	public Pad pad;
	public User user;
	
	public void loadPreferences() {

		SharedPreferences preferences = getApplicationContext().getSharedPreferences( "user" 
				, Context.MODE_PRIVATE);
		
		pad = new Pad();
		user = new User( preferences.getString("user_name", null), null);
	}
}
