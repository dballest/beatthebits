package helpers;

import java.util.ArrayList;

import models.Pad;
import models.Tile;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class MySQLiteHelper extends SQLiteOpenHelper {

	// Database Version
	private static final int DATABASE_VERSION = 2;
	// Database Name
	private static final String DATABASE_NAME = "btb";

	public MySQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);  
	}


	// Pads table name
	private static final String TABLE_PADS = "pads";
	private static final String TABLE_TILES = "tiles";

	// Pads Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_NAME = "name";
	private static final String KEY_AUTHOR = "author";

	private static final String[] PAD_COLUMNS = {KEY_ID,"size",KEY_NAME,"color",KEY_AUTHOR};
	private static final String[] TILE_COLUMNS = {"id","pad_id","tile_id","track"};

	@Override
	public void onCreate(SQLiteDatabase db) {
		// SQL statement to create tables
		String CREATE_PAD_TABLE = "CREATE TABLE IF NOT EXISTS pads ( " +
				"id INTEGER PRIMARY KEY AUTOINCREMENT, " + 
				"size INTEGER, "+
				"name TEXT, "+
				"color TEXT, "+
				"author TEXT )";

		String CREATE_TILE_TABLE = "CREATE TABLE IF NOT EXISTS tiles ( " +
				"id INTEGER PRIMARY KEY AUTOINCREMENT, " + 
				"pad_id INTEGER, "+
				"tile_id INTEGER, "+
				"track TEXT )";

		// create tables
		db.execSQL(CREATE_PAD_TABLE);
		db.execSQL(CREATE_TILE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older pads table if existed
		db.execSQL("DROP TABLE IF EXISTS pads");
		db.execSQL("DROP TABLE IF EXISTS tiles");
		// create fresh pads table
		this.onCreate(db);
	}

	public void addPad(Pad pad){
		//for logging
		Log.d("addPad", pad.toString()); 

		// 1. get reference to writable DB
		SQLiteDatabase db = this.getWritableDatabase();

		// 2. create ContentValues to add key "column"/value
		ContentValues values = new ContentValues();
		values.put("size", pad.getSize());
		values.put(KEY_NAME, pad.getName()); // get title
		values.put("color", pad.getColor()); // get title
		values.put(KEY_AUTHOR, pad.getAuthor()); // get author

		// 3. insert
		db.insert(TABLE_PADS, null, //nullColumnHack
				values); // key/value -> keys = column names/ values = column values

		// 4. close
		db.close(); 
	}

	public void addTile(Tile tile){

		Log.d("addTile", tile.toString()); 
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("tile_id", tile.getIndex());
		values.put("pad_id", tile.getPadId()); 
		values.put("track", tile.getTrack());
		db.insert(TABLE_TILES, null, values);
		db.close(); 
	}

	public Pad getPad(int id){ // 1. get reference to readable DB
		SQLiteDatabase db = this.getReadableDatabase();

		// 2. build query
		Cursor cursor = 
				db.query(TABLE_PADS, // a. table
						PAD_COLUMNS, // b. column names
						" id = ?", // c. selections 
						new String[] { String.valueOf(id) }, // d. selections args
						null, // e. group by
						null, // f. having
						null, // g. order by
						null); // h. limit

		// 3. if we got results get the first one
		Pad pad = null;
		if ( cursor.moveToFirst() ) {

			// 4. build pad object
			pad = new Pad();
			pad.setId(Integer.parseInt(cursor.getString(0)));
			pad.setSize(Integer.parseInt(cursor.getString(1)));
			pad.setName(cursor.getString(2));
			pad.setColor(cursor.getString(3));
			pad.setAuthor(cursor.getString(4));

			//log 
			Log.d("getPad("+id+")", pad.toString());
		} else {
			Log.d("getPad("+id+")", "No pad with such id found");
		}

		// 5. return pad & close cursor properly
		cursor.close();
		return pad;
	}

	public Tile getTile(int id){ 

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = 
				db.query(TABLE_TILES,
						TILE_COLUMNS, 
						" id = ?", // c. selections 
						new String[] { String.valueOf(id) }, // d. selections args
						null, // e. group by
						null, // f. having
						null, // g. order by
						null); // h. limit

		if (cursor != null)
			cursor.moveToFirst();

		Tile tile = new Tile();
		tile.setId(Integer.parseInt(cursor.getString(0)));
		tile.setPadId(Integer.parseInt(cursor.getString(1)));
		tile.setIndex(Integer.parseInt(cursor.getString(2)));
		tile.setTrack(cursor.getString(3));

		//log 
		Log.d("getTile("+id+")", tile.toString());

		// 5. return pad & close cursor properly
		cursor.close();
		return tile;
	}

	public ArrayList<Pad> getAllPads() {

		ArrayList<Pad> pads = new ArrayList<Pad>();

		// 1. build the query
		String query = "SELECT  * FROM " + TABLE_PADS;

		// 2. get reference to writable DB
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		// 3. go over each row, build pad and add it to list
		Pad pad = null;
		if (cursor.moveToFirst()) {
			do {
				pad = new Pad();
				pad.setId(Integer.parseInt(cursor.getString(0)));
				pad.setSize(Integer.parseInt(cursor.getString(1)));
				pad.setName(cursor.getString(2));
				pad.setColor(cursor.getString(3));
				pad.setAuthor(cursor.getString(4));

				// Add pad to pads
				pads.add(pad);
			} while (cursor.moveToNext());
		}

		Log.d("getAllPads()", pads.toString());

		db.close();
		cursor.close();
		return pads;
	}	

	public Tile[] getPadTiles( Pad pad ){

		int N = pad.getSize() * pad.getSize();
		Tile[] tiles = new Tile[N];

		SQLiteDatabase db = this.getWritableDatabase();

		final String[] QUERY_TARGET = new String[] { String.valueOf( pad.getId() ) };
		final String QUERY_LIMIT = String.valueOf( N );

		Cursor cursor = 
				db.query(TABLE_TILES, // a. table
						TILE_COLUMNS, // b. column names
						" pad_id = ?", // c. selections 
						QUERY_TARGET, // d. selections args
						null, // e. group by
						null, // f. having
						null, // g. order by
						QUERY_LIMIT ); // h. limit

		Tile tile = null;
		if (cursor.moveToFirst()) {
			do {

				tile = new Tile();
				tile.setId(Integer.parseInt(cursor.getString(0)));
				tile.setPadId(Integer.parseInt(cursor.getString(1)));
				tile.setIndex(Integer.parseInt(cursor.getString(2)));
				tile.setTrack(cursor.getString(3));

				// Add pad to pads
				tiles[ tile.getIndex() ] = tile;
				Log.d("getPadTiles()", tile.toString());

			} while (cursor.moveToNext());
		}

		cursor.close();
		return tiles;
	}

	public void deletePad(Pad pad) {

		// 1. get reference to writable DB
		SQLiteDatabase db = this.getWritableDatabase();

		// 2. Borrar todos los Tile asociados
		Tile[] tiles = getPadTiles(pad);
		for( int j = 0; j < pad.getSize(); j++ ) {
			if( tiles[j] != null ) {
				String[] id = new String[] { String.valueOf( tiles[j].getId()) };
				db.delete(TABLE_TILES, KEY_ID+" = ?", id);
				Log.d("deleteTile", tiles[j].toString());
			}
		}

		// 3. delete
		db.delete(TABLE_PADS, //table name
				KEY_ID+" = ?",  // selections
				new String[] { String.valueOf(pad.getId()) }); //selections args

		// 4. close
		db.close();
		Log.d("deletePad", pad.toString());

	}

	public void deleteTile(Tile tile){
		if( tile != null ) {
			SQLiteDatabase db = this.getWritableDatabase();
			db.delete(TABLE_TILES, //table name
					KEY_ID+" = ?",  // selections
					new String[] { String.valueOf(tile.getId()) }); //selections args
			db.close();
			Log.d("deleteTile", tile.toString());
		}
	}

	public Pad getPad( String name ) {
		SQLiteDatabase db = this.getReadableDatabase();

		// 2. build query
		Cursor cursor = 
				db.query(TABLE_PADS, // a. table
						PAD_COLUMNS, // b. column names
						" name = ?", // c. selections 
						new String[] { name }, // d. selections args
						null, // e. group by
						null, // f. having
						null, // g. order by
						null); // h. limit

		// 3. if we got results get the first one
		Pad pad = null;
		if ( cursor.moveToFirst() ) {

			// 4. build pad object
			pad = new Pad();
			pad.setId(Integer.parseInt(cursor.getString(0)));
			pad.setSize(Integer.parseInt(cursor.getString(1)));
			pad.setName(cursor.getString(2));
			pad.setColor(cursor.getString(3));
			pad.setAuthor(cursor.getString(4));

			//log 
			Log.d("getPad("+name+")", pad.toString());
		} else {
			Log.d("getPad("+name+")", "No pad with such name found");
		}

		// 5. return pad & close cursor properly
		cursor.close();
		return pad;		
	}
}
