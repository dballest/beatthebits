package models;

import helpers.MySQLiteHelper;
import android.content.Context;


public class Pad {

	private int id;
	private int size = 3;
	private String name = "Awesome Pad";
	private String author = "you";
	private String color = "#C52F24";
	private Tile tiles[];
	/* Handler de la base de datos */

	/* Constructores */
	public Pad(){}

	public Pad(int id){
		this.id = id;
	}

	public Pad(String name, String author){
		this.name = name;
		this.author = author;
	}

	/* Setters & getters */

	public void setName( String name ){
		this.name = name;
	}

	public String getName() { 
		return this.name;
	}

	public void setId( int id ){ 
		this.id = id;
	}

	public int getId(){ 
		return this.id;
	}

	public void setAuthor( String author ) { 
		this.author = author;
	}

	public String getAuthor() { 
		return this.author;
	}

	@Override 
	public String toString() {
		return "id: " + id +"; pad_name: " + name + "; author: " + author + "; color: "+ color;
	}

	public void setColor( String color ){
		this.color = color;
	}

	public String getColor() {
		return this.color;
	}

	public int getSize(){
		return this.size;
	}

	public void setSize(int size){
		if (size == 4)//proteccion contra tamanios indeseados
			this.size = size;
	}

	public void loadTilesFromDatabase( Context context ) {
		MySQLiteHelper db = new MySQLiteHelper(context);
		tiles = db.getPadTiles( this );
		db.close();
	}

	public Tile getTile( int index ) {
		if (tiles == null)
			return null;
		else
			return tiles[index]; 
	}
}
