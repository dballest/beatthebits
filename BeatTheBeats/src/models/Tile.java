package models;


public class Tile {

	int id;
	int index;
	int pad_id;
	String track;
	
	/* Constructores */
	public Tile(){}
	
	public Tile(int index, int pad_id, String track) {
		this.index = index;
		this.pad_id = pad_id;
		this.track = track;
	}
	
	/* Setters & Getters*/
	public int    getId() { 
		return this.id;
	}
	public void   setId( int e ) { 
		this.id = e;
	}
	public int    getIndex() { 
		return this.index;
	}
	public void   setIndex( int e ) { 
		this.index = e;
	}
	public void   setPadId( int id ) {
		this.pad_id = id;
	}
	public int getPadId( ) {
		return this.pad_id;
	}
	public void setTrack( String path ) {
		this.track = path;
	}
	public String getTrack() {
		return this.track;
	}

	@Override
	public String toString(){
		return "id: "+id+"; index: "+index+"; pad_id: "+pad_id+"; track: "+track+";";
	}
}
