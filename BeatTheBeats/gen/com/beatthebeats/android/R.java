/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.beatthebeats.android;

public final class R {
    public static final class anim {
        public static final int slide_in=0x7f040000;
        public static final int slide_out=0x7f040001;
    }
    public static final class array {
        public static final int languages_arrays=0x7f080000;
        public static final int pad_sizes_array=0x7f080001;
    }
    public static final class attr {
    }
    public static final class color {
        public static final int backBlue_4_textFields=0x7f050001;
        public static final int backgray_4_textfields=0x7f050003;
        public static final int bg_gradient_end=0x7f050014;
        public static final int bg_gradient_start=0x7f050013;
        /**  Our APP colors 
         */
        public static final int blue_button=0x7f05000f;
        public static final int green_4_background=0x7f050000;
        public static final int green_button=0x7f050011;
        public static final int green_tile=0x7f05000b;
        public static final int green_tile_dark=0x7f05000c;
        public static final int inactive_tile=0x7f050008;
        /**  Our PAD colors 
         */
        public static final int pad_background=0x7f050004;
        public static final int playable_tile=0x7f050005;
        public static final int playable_tile_dark=0x7f050006;
        public static final int playing_tile=0x7f050009;
        public static final int playing_tile_dark=0x7f05000a;
        public static final int purple_tile=0x7f05000d;
        public static final int purple_tile_dark=0x7f05000e;
        public static final int red_button=0x7f050010;
        public static final int selectable_tile=0x7f050007;
        public static final int white=0x7f050002;
        public static final int yellow_button=0x7f050012;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f060000;
        public static final int activity_vertical_margin=0x7f060001;
        public static final int corner_radius=0x7f060002;
        public static final int title_text_size=0x7f060003;
    }
    public static final class drawable {
        public static final int add_button=0x7f020000;
        public static final int alien=0x7f020001;
        public static final int alien2=0x7f020002;
        public static final int alien3=0x7f020003;
        public static final int avatar_button=0x7f020004;
        public static final int bg_gradient=0x7f020005;
        public static final int blue_btn=0x7f020006;
        public static final int cool=0x7f020007;
        public static final int dflt_avatar=0x7f020008;
        public static final int fav=0x7f020009;
        public static final int ic_action_edit=0x7f02000a;
        public static final int ic_action_remove=0x7f02000b;
        public static final int inactive_tile=0x7f02000c;
        public static final int login_logo=0x7f02000d;
        public static final int pacman1=0x7f02000e;
        public static final int pad_bg=0x7f02000f;
        public static final int playable_tile=0x7f020010;
        public static final int playing_tile=0x7f020011;
        public static final int red_btn=0x7f020012;
        public static final int save_prof_changes=0x7f020013;
        public static final int selectable_tile=0x7f020014;
        public static final int set_avatar=0x7f020015;
        public static final int textfields_back_1=0x7f020016;
    }
    public static final class id {
        public static final int Switch01=0x7f0b0002;
        public static final int action_edit=0x7f0b001d;
        public static final int action_logout=0x7f0b001c;
        public static final int action_logout2=0x7f0b001a;
        public static final int action_profile=0x7f0b001f;
        public static final int action_profile2=0x7f0b0018;
        public static final int action_remove=0x7f0b001e;
        public static final int action_settings=0x7f0b001b;
        public static final int action_settings2=0x7f0b0019;
        public static final int addBtn=0x7f0b0017;
        public static final int avatarButton=0x7f0b000c;
        public static final int button2=0x7f0b0003;
        public static final int email_field=0x7f0b0010;
        public static final int list=0x7f0b000a;
        public static final int login_btn=0x7f0b0008;
        public static final int login_password=0x7f0b0007;
        public static final int login_username=0x7f0b0006;
        public static final int name_field=0x7f0b000e;
        public static final int off_session_btn=0x7f0b0009;
        public static final int pad_sizes_spinner=0x7f0b0016;
        public static final int pad_table=0x7f0b000b;
        public static final int pad_title=0x7f0b0015;
        public static final int phone_field=0x7f0b0011;
        public static final int row_title=0x7f0b0014;
        public static final int settings_track=0x7f0b0004;
        public static final int spinner1=0x7f0b0000;
        public static final int surname_field=0x7f0b000f;
        public static final int switch1=0x7f0b0001;
        public static final int textView1=0x7f0b0005;
        public static final int track_list=0x7f0b0013;
        public static final int track_list_btn=0x7f0b0012;
        public static final int username_field=0x7f0b000d;
    }
    public static final class layout {
        public static final int activity_general_settings=0x7f030000;
        public static final int activity_login=0x7f030001;
        public static final int activity_main_page=0x7f030002;
        public static final int activity_pad=0x7f030003;
        public static final int activity_profile=0x7f030004;
        public static final int activity_track_list=0x7f030005;
        public static final int list_row=0x7f030006;
        public static final int pad_size_selector=0x7f030007;
    }
    public static final class menu {
        public static final int home_menu=0x7f0a0000;
        public static final int main_menu=0x7f0a0001;
        public static final int pad_menu=0x7f0a0002;
    }
    public static final class string {
        public static final int action_create_pad=0x7f070002;
        public static final int action_edit=0x7f070004;
        public static final int action_logout=0x7f070001;
        /**  Menu 
         */
        public static final int action_profile=0x7f070000;
        public static final int action_remove=0x7f070005;
        public static final int action_settings=0x7f070007;
        /**  Profile Settings 
         */
        public static final int app_name=0x7f070006;
        public static final int chngPassButton=0x7f07000e;
        public static final int email=0x7f07000c;
        /**  General Settings 
         */
        public static final int language_prompt=0x7f07001a;
        public static final int login_btn=0x7f070008;
        public static final int mailLoginField=0x7f070010;
        public static final int menu_item_profile=0x7f070014;
        public static final int menu_item_settings=0x7f070015;
        public static final int name=0x7f07000a;
        public static final int noLoginButt=0x7f070012;
        public static final int notification_setting=0x7f07001e;
        /**  Pad Creation 
         */
        public static final int pad_name_creator=0x7f07001c;
        public static final int pad_size_prompt=0x7f07001b;
        /**  Pad menu 
         */
        public static final int pad_title=0x7f070003;
        public static final int passLoginField=0x7f070011;
        public static final int phone=0x7f07000d;
        /**  Colors 4 rows 
         */
        public static final int red_color=0x7f07001f;
        public static final int saveProfileEdition=0x7f07000f;
        public static final int surname=0x7f07000b;
        public static final int sync_locally=0x7f07001d;
        public static final int title_activity_general_settings=0x7f070017;
        public static final int title_activity_main_page=0x7f070018;
        public static final int title_activity_pad=0x7f070013;
        public static final int title_activity_profile=0x7f070019;
        /**  Track List Activity 
         */
        public static final int title_activity_track_list=0x7f070016;
        public static final int username=0x7f070009;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f090000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f090002;
        public static final int AvatarButton=0x7f09000d;
        /**  PROFILE ACTIVITY 
         */
        public static final int MainFieldsEditProfile=0x7f090009;
        public static final int MyActionBar=0x7f090001;
        public static final int SecondaryFieldsEditProfile=0x7f09000a;
        /**  botones 
 _btn es el generico 
         */
        public static final int _btn=0x7f090005;
        public static final int blue_btn=0x7f090006;
        public static final int changePassButton=0x7f09000b;
        public static final int dflt_text_field=0x7f090004;
        public static final int red_btn=0x7f090007;
        public static final int saveProfChangesButton=0x7f09000c;
        public static final int text_field=0x7f090003;
        public static final int tile=0x7f090008;
    }
    public static final class styleable {
        /** Attributes that can be used with a PadMatrix.
         */
        public static final int[] PadMatrix = {
            
        };
        /** Attributes that can be used with a TileButton.
         */
        public static final int[] TileButton = {
            
        };
    };
}
